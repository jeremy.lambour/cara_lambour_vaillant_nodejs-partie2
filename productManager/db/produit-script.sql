create table produits(
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    nom TEXT, 
    description TEXT, 
    prix INT, 
    src TEXT
);

INSERT INTO produits(nom,description,prix,src) VALUES ("chocolat de noël","Coffret reglette transparent contenant 6 timbales au décor festif contenant : des galettes au beurre personnalisées avec des décors festifs ainsi que des billes de céréales enrobées de chocolat, cacahuètes grillées, guimauves maison, coeurs nougatine enrobés de chocolat....
Dimensions environ 13x17x6cm - 355g
Prêt à offrir
Fabriqué de facon artisanale dans notre chocolatier en vendée à la Roche sur Yon
Chocodic Maître artisan chocolatier depuis 20 ans",14.99,"images/chocolat.jpg" );

INSERT INTO produits(nom,description,prix,src) VALUES ("Nerf - Rival Kronos XVIII - E0005","Le Nerf Rival Kronos XVIII-500 possède une grande puissance malgré son design compact.
Capacité : 5 billes en mousse (incluses).
Mécanisme à ressort, rail tactique et détente avec verrou.
Puissance : jusqu’à 100 km/h.",9.99,"images/nerf.jpg" );


INSERT INTO produits(nom,description,prix,src) VALUES ("BenQ Eye-Care GW2270H 21'5 pouces, FHD, VGA, 2 x HDMI 1.4, Flicker-Free, Low Blue Light
","Description du produit : Benq GW2270H Écran LED pour PC
Taille de l'écran: 54,6 cm 
Luminosité: 250 cd/m². Gamme de couleurs: 72%
Résolution de l'écran: 1920 x 1080 pixels
Casque connection: 3,5 mm
Couleur: Noir ; Couleurs pieds: Noir
Angle d'inclinaison: -5 - 20°",99.95,"images/ecran.jpg" );

INSERT INTO produits(nom,description,prix,src) VALUES ("
Logitech G430 Casque Gaming ","Des commandes audio à portée de main. Le commutateur de sourdine et le contrôle du volume à portée de main vous permettent de régler le son rapidement et avec précision
Entendez vos ennemis approcher. Vous pouvez recevoir jusqu'à sept canaux audio distincts ainsi qu'un canal d'effets de basse fréquence (LFE), désigné sous le nom de son surround 7.1.
Confort optimisé. Les oreillettes du casque gamer G430 sont recouvertes d'un tissu sportswear pour une sensation douce et confortable, même après plusieurs heures d'utilisation",39.95,"images/casque.jpg" );