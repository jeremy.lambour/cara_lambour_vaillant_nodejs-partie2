//Module base de données
var sqlite = require("sqlite3");
var db = new sqlite.Database("./db/db-cara-nodejs.db3");

/* Products */
function insertProduct(nom,description,prix,src)
{
    db.run("insert into produits values(?,?,?,?)",[nom,description,prix,src]);
}

function selectAllProduct(cb)
{
    /* TODO 
    *  Recupere l'ensemble des produits de la table produits 
    */
}

function selectProductById (id)
{
    db.all("SELECT * FROM produits where id = ?", [id], function(err,rows) {
        console.log("Le produit " + rows[0].nom + ". Description : " + rows[0].description);
    });
}

//Export fonctions base produits
exports.insertProduct = insertProduct;
exports.selectAllProduct = selectAllProduct;
exports.selectProductById = selectProductById;

