var express = require("express");
var router = express.Router();
var db = require("../db/db-users-nodejs");

router.get("/", function(req, res) {
  res.render("view-login.ejs");
});

//TODO affecter une route /register à la template view-register

//TODO affecter une route /registered qui va insérer un utilisateur et faire une redirection vers la vue login

//TODO affectuer une route login qui permet d'authentifier l'utilisateur via une vérification dans la bd et redirige l'utilisateur vers /home si celui ci existe


module.exports = router;
